# Notas sobre a Linguagem C

1. [x] [Hello, World!](#hello-world)

## Hello, World! <a name="hello-world"></a>

### O que esperar de C.
C é uma linguagem de baixo nível.

### Hello, World!

Seguindo a tradição, o primeiro programa será o que apenas imprime na tela a mensagem "Hello, World!".


~~~c
// arquivo:  hello.c
// programa: Hello World

#include <stdio.h>           // header file

int main(void) {
  printf("Hello, World!\n);
  return 0;
}
~~~

No terminal o seguinte comando para compilar o programa.

```bash
gcc -Wextra -Werror -Wall -pedantic -std=c2x -o hello hello.c
```

Para executar o programa Hello World.
```bash
./hello
```
Feito isso, será impresso a seguinte mensagem no terminal.
```bash
Hello, World!
```

## Referências
 - Beej's Guide to C Programming \
  https://beej.us/guide/bgc/
 - Beej's Guide to C Programming—Library Reference \
   https://beej.us/guide/bgclr/
